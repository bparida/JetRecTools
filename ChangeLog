2016-10-26    <delsart@lpsc1120x.in2p3.fr>

	* Root/LinkDef.h: adding entries in the dict.
	* Tagging JetRecTools-00-03-58

2016-10-13  Clement Camincher  <ccaminch@cern.ch>

	* cmt/Makefile.RootCore: Add packages dependances
	* Tagging JetRecTools-00-03-57

2016-10-11  Johannes Elmsheuser  <Johannes.Elmsheuser@cern.ch>
	* Add python/DefaultTools.py
	* Tagging JetRecTools-00-03-56

2016-10-10  Johannes Elmsheuser  <Johannes.Elmsheuser@cern.ch>
	* port latest config for JetRecConfig
	* Tagging JetRecTools-00-03-55

2016-10-7  Clement Camincher  <ccaminch@lxplus084.cern.ch>

	* fixed componenet
	* Tag as JetRecTools-00-03-54

2016-10-04  Clement Camincher  <ccaminch@lxplus084.cern.ch>

	* fixed requirement
	* Tag as JetRecTools-00-03-53

2016-10-04  Clement Camincher  <ccaminch@lxplus109.cern.ch>

	* add JetInputElRemovalTool
	* Tag as JetRecTools-00-03-52

2016-10-03    <khoo@cern.ch>
	* Root/PFlowPseudoJetGetter.cxx: Compare with -FLT_MIN rather than with 0
	* Tag as JetRecTools-00-03-51

2016-09-12    <khoo@cern.ch>
	* Root/PFlowPseudoJetGetter.cxx: Update to WeightPFOTool interface
	* Tag as JetRecTools-00-03-50

2016-09-10    <khoo@cern.ch>
	* Root/PFlowPseudoJetGetter.cxx: initial change to retain CPFOs with weight 0 as jet constituents
	* Tag as JetRecTools-00-03-49

2016-09-10    <delsart@lpsc1120x.in2p3.fr>

	* Root/LinkDef.h: added
	* Tag as JetRecTools-00-03-48

2016-08-31  scott snyder  <snyder@bnl.gov>

	* Tagging JetRecTools-00-03-47.
	* Comply with ATLAS naming conventions.

2016-08-23  scott snyder  <snyder@bnl.gov>

	* Tagging JetRecTools-00-03-46.
	* Fix compilation warning.

2016-08-19    <delsart@lpsc1120x.in2p3.fr>

	* Root/CaloClusterConstituentsOrigin.cxx
	JetConstituentModSequence.cxx cmt/Makefile.RootCore :
	compatibility with RootCore
	* Tag as JetRecTools-00-03-45

2016-08-17 TJ Khoo
	M       Root/CaloClusterConstituentsOrigin.cxx
	* Don't modify -ve energy clusters
	* Tag as JetRecTools-00-03-44

2016-08-04 TJ Khoo
	M       cmt/requirements
	M       JetRecTools/PFlowPseudoJetGetter.h
	M       Root/PFlowPseudoJetGetter.cxx
	* Move charged PFO weight computation to PFlowUtils
	* tag as JetRecTools-00-03-43

2016-08-01 TJ Khoo
	* Actually add the ConstituentSubtractorTool files
	* tag as JetRecTools-00-03-42

2016-07-31 TJ Khoo
	* tag as JetRecTools-00-03-41

2016-07-27    <delsart@lpsc1120x.in2p3.fr>

	* VoronoiWeightTool.h/cxx : clean-up header, remove unnecessary
	mutable, comply to Atlas coding style.
	* add ConstituentSubtractorTool, adapt requirements and CMakeLists

2016-05-31    <delsart@lpsc1120x.in2p3.fr>

	* Several changes to fully enable constituent modifiers in reco.
	M       python/JetRecToolsConfig.py
	M       Root/CaloClusterConstituentsOrigin.cxx
	M       Root/JetConstituentModSequence.cxx

	M       JetRecTools/CaloClusterConstituentsOrigin.h
	M       JetRecTools/JetConstituentModSequence.h
	* ATLASRECTS-2820

2016-05-24 TJ Khoo
	M       cmt/requirements
	M       src/components/JetRecTools_entries.cxx
	M       CMakeLists.txt
	* ATLASRECTS-2823
	* Manual merge with trunk

2016-05-04 James Frost o.b.o. Daniel Whiteson
	* ATLASRECTS-2822
	* Root/CaloClusterConstituentsOrigin.cxx: add cluster origin correction
	* JetRecTools/CaloClusterConstituentsOrigin.h: add cluster origin correction
	* src/components/JetRecTools_Entries.cxx add above
	* tag as JetRecTools-00-03-37

2016-05-03    <delsart@localhost>
	* ATLASRECTS-2821
	* SoftKillerWeightTool  and config  add default value to
	SKGridSize.
	* NO TAG YET

2016-05-02    <delsart@localhost>
	* ATLASRECTS-2824
	* python/JetRecToolsConfig.py: adding minimal configuration for
	modifier sequence.
	* SoftKillerWeightTool is not yet properly configured !!
	* SoftKillerWeightTool.cxx : regular indentation.
	* Tag as JetRecTools-00-03-36

2016-04-30 James Frost (james.frost AT cern.ch)
	* CMakeLists.txt: Try (unsucessfully) to fix cmakelist for JetRecTools
	* ATLASRECTS-2821
	* NO TAG YET

2016-04-29 Jennifer Roloff
	* ATLASRECTS-2821
	* JetRecTools/SoftKillerWeightTool.h : add SK tool
	* Root/SoftKillerWeightTool.cxx : add SK tool
	* cmt/requirements : add dependency on fastjet, contribs, and SK
	* src/components/JetRecTools_entries.cxx : add SK tool

2016-04-28 James Frost ( james.frost AT cern.ch)
	* Root/JetConstituentModSequence.cxx: fix unchecked process() method

2016-04-25 James Frost ( james.frost AT cern.ch)
	* Fix Cmake builds
	* Tag as JetRecTools-00-03-34

2016-04-11 Michael Nelson
	* ATLASRECTS-2824
	* trunk/Root/JetConstituentModSequence.cxx: file added (sequencer)
	* trunk/Root/CaloClusterConstituentsWeight.cxx: file added (example
			implementation)
	* trunk/Root/JetConstituentModifierBase.cxx: file added (base class)
	* trunk/JetRecTools/JetConstituentModSequence.h: file added (sequencer)
	* trunk/JetRecTools/CaloClusterConstituentsWeight.h: file added (example
			implementation)
	* trunk/JetRecTools/JetConstituentModifierBase.h: file added (base class)
  * Tag as JetRecTools-00-03-33

2016-03-10 Mark Hodgkinson
	* Root/PFlowPseudoJetGetter.cxx: Bug fix to avoid unphysical weights applied to charged PFO in non-dense environments
	* Tag as JetRecTools-00-03-32

2015-10-13 Mark Hodgkinson
	* Root/PFlowPseudoJetGetter.cxx: Bug fix s that if there is no primary vertex, we use NoPV default vertex
	* ATLASRECTS-2522
	* Tag as JetRecTools-00-03-31

2015-10-05 Mark Hodgkinson
	* Root/PFlowPseudoJetGetter.cxx: Bug fix so that tracks can be associated to the primary vertex
	* Tag as JetRecTools-00-03-30

2015-10-02 Mark Hodgkinson
	* Root/PFlowPseudoJetGetter.cxx,JetRecTools/PFlowPseudoJetGetter.h: Update to add option to use track-vertex association tool
	* ATLJETMET-442
	*  Tag as JetRecTools-00-03-29
	
2015-10-01 Mark Hodgkinson
	* JetRecTools/PFlowPseudoJetGetter.h,  Root/PFlowPseudoJetGetter.cxx: Make new charged PFO weights optional
	* ATLJETMET-425
	* python/JetRecToolsConfig.py: Remove commented out python module configuration code
	* Tag as JetRecTools-00-03-28

2015-10-01 Mark Hodgkinson
        * Merge 00-03-23-branch onto trunk
	* Root/PFlowPseudoJetGetter.cxx : Remove cout + migrate to new PFO variable names
        * Tag as JetRecTools-00-03-23-02  (based on JetRecTools-00-03-23-branch)
	* Root/PFlowPseudoJetGetter.cxx : Add weights to charged PFO
        * ATLJETMET-425
        * Tag as JetRecTools-00-03-23-01 (based on JetRecTools-00-03-23-branch)

2015-07-20 Mark Hodgkinson
	* Root/PFlowPseudoJetGetter.cxx: Bug fix to account for rare events where 0th vertex is not a primary vertex
	* ATLASRECTS-2223
	* Tag as JetRecTools-00-03-27
	
2015-06-17  Jakub Cuth
        * ATLJETMET-88: no change just tagging
	* tagging as JetRecTools-00-03-26: working for Base,2.3.X

2015-05-28  Jakub Cuth
        * ATLJETMET-88
	* cmt/Makefile.RootCore: Adding the file with dependencies
	* JetRecTools/Root/TrackVertexAssociationTool.cxx: hotfix of
	reference-to-pointer (due to old version of InDet TVA tool). This
	change is not in trunk only in tag. Working for Base,2.2.X
	* tagging as JetRecTools-00-03-25.

2015-04-24  scott snyder  <snyder@bnl.gov>

	* Tagging JetRecTools-00-03-24.
	* cmt/requirements: Fix checkreq warnings.

2015-04-20 Mark Hodgkinson
	* Tagging JetRecTools-00-03-23
	* Update to use new interface of m_tvaTool in TrackVertexAssociationTool.cxx

2015-03-31  scott snyder  <snyder@bnl.gov>

	* Tagging JetRecTools-00-03-22.
	* Root/PFlowPseudoJetGetter.cxx: Fix clang warning (struct/class
	inconsistency).

2015-02-09 Mark Hodgkinson
	* JetRecTools-00-03-21	
	* ATLJETMET-217
	* Bug fix in PFlowPseudoJetGetter so that -ve energy pfo are not used.

2015-02-05 Mark Hodgkinson
	* JetRecTools-00-03-20
	* ATLJETMET-220
	* Add new option UseVertices to PFlowPseduoJetGetter and use it to disable use of vertices in pflow for cosmics

2015-01-12    <delsart@localhost>
	* ATLJETMET-180
	* SimpleJetTrackSelectionTool, JetTrackSelectionTool,
	TrackVertexAssociationTool SimpleJetTrackSelectionTool : fixed compilation in AthAnalysisBase.
	* MissingCellListTool moved to new JetRecCalo package.
	* Tagging JetRecTools-00-03-19

2014-12-19  scott snyder  <snyder@bnl.gov>

	* Tagging JetRecTools-00-03-18.
	* Fix checkreq warnings.

2014-12-18 David Quarrie <David.Quarrie@cern.ch>
	* cmt/requirements
		Add cmake-specific explicit dependency against xAODPFlow to avoid indirect dependency from PFlowUtils
		which creates a component library and cmake (correctly) does not propagate such dependencies. This is
		transparent to CMT and checkreq.
	* Tagged as JetRecTools-00-03-17

2014-12-08 David Adams
	* JetRecTools-00-03-16
	* ATLASRECTS-1345
	* Clean up ChangeLog.

2014-12-08 Mark Hodgkinson
	* JetRecTools-00-03-14
	* ATLASRECTS-1345
	* Fix memory leak in PFlowPseudoJetGetter - container returned by
	  RetrievePFOTool should be deleted.

2014-12-04    <delsart@localhost>
	* JetRecTools-00-03-13
	* ATLJETMET-29
	* TrackVertexAssociationTool : Can make use of
	ITrackVertexAssociationTool.

2014-11-06    <delsart@localhost>
	* JetRecTools-00-03-12
	* ATLJETMET-79
	* TrackVertexAssociationTool : add the option to cut on
	Z0*sin(theta) instead of z0. 
	* Set default cut values to accept anything (thus all tracks are
	associated to PV0).

2014-11-05 David Adams
	* JetRecTools-00-03-11
	* ATLJETMET-80
	* PFlowPseudoJetGetter: Drop requirement that neutral pflow have at
	  least one entry.

2014-10-28 David Adams
	* JetRecTools-00-03-10
	* ATLJETMET-61
	* SimpleJetTrackSelectionTool: Add keep(...) from IJetTrackSelector.
	* JetTrackSelectionTool: Add keep(...) from IJetTrackSelector.

2014-10-28 David Adams
	* JetRecTools-00-03-09
	* ATLJETMET-61
	* JetTrackSelectionTool: Modify to use the new InDet track selection
	  interface, drop pT and eta cuts and reject null pointers.
	* SimpleJetTrackSelectionTool: New track selector that keeps all but
	  null pointers and tracks below a pT threshold.
	* JetDetailedTrackSelectorTool: Remove this.

2014-10-28 Mark Hodgkinson
	* JetRecTools-00-03-08
	* PFlowPseudoJetGetter: Update (this time correctly)  z0 cut to take into account primary vertex

2014-10-28 David Adams
	* JetRecTools-00-03-07
	* PFlowPseudoJetGetter: Fis use of UseCharged.

2014-10-27 Mark Hodgkinson
	* JetRecTools-00-03-06
	* PFlowPseudoJetGetter: Update z0 cut to take into account primary vertex
	
2014-10-27 David Adams
	* JetRecTools-00-03-05
	* PFlowPseudoJetGetter: Add properties UseNeutral and UseCharged.

2014-10-22  David Quarrie <David.Quarrie@cern.ch>
	* cmt/requirements
		Add explicit dependency against xAODPFlow to avoid indirect dependency from PFlowUtils
		which creates a component library and cmake (correctly) does not propagate such dependencies
	* Tagged as JetRecTools-00-03-04

2014-09-04  scott snyder  <snyder@bnl.gov>
	* Tagging JetRecTools-00-03-03.
	* cmt/requirements: Fix checkreq warnings.

2014-08-26    <delsart@localhost>
	* Root/PFlowPseudoJetGetter.cxx : filter E<=0 instead of E<0 
	* JetRecTools-00-03-02

2014-05-27    <delsart@localhost>
	* Root/PFlowPseudoJetGetter.cxx : protect against invalid 4-vectors 
	* JetRecTools-00-03-01

2014-03-20 David Adams
	* JetRecTools-00-03-00
	* PFlowPseudoJetGetter: Add support for LC and EM->LC.

2014-05-19    <delsart@localhost>
	* Root/PFlowPseudoJetGetter.cxx : skip neg energy

2014-05-16    <delsart@localhost>
	* Root/PFlowPseudoJetGetter.cxx : bug fix.
	* tag JetRecTools-00-02-08

2014-05-15    <delsart@localhost>
	* PFlowPseudoJetGetter : added. Not tested yet, no tag. 

2014-03-31    <delsart@localhost>
	* Root/TrackVertexAssociationTool.cxx : more debug info. no tag.  

2014-03-20 David Adams
	* JetRecTools-00-02-07
	* Root/TrackPseudoJetGetter.cxx: Add missing return.

2014-03-20    <delsart@localhost>
	* Root/TrackPseudoJetGetter.cxx : workaround temporary limitation
	where TrackParticleContainer can not be retrieved as
	IParticleContainer
	* Root/TrackVertexAssociationTool : re-write long distance
	calculation
	* tag 	JetRecTools-00-02-06

2014-03-19 David Adams
	* JetRecTools-00-02-05
	* TrackVertexAssociationTool: Add check that track container owns its
	  elements. Otherwise one can expect a crash down the line.

2014-03-10 David Adams
	* JetRecTools-00-02-04
	* JetDetailedTrackSelectorTool: Add this to use as replacement
	  for InDetTrackSelectorTool to allow running w/o RecExCommon.
	* src/components/JetRecTools_entriex.cxx: Add the preceding.

2014-03-10    <delsart@localhost>
	* added MissingCellListTool 
	* added python/JetRecToolsConfig.py
	* adapated requirements
	* tag JetRecTools-00-02-03

2014-02-28    <delsart@localhost>
	* added TrackPseudoJetGetter.cxx 
	* tag JetRecTools-00-02-02

2014-02-26    <delsart@localhost>
	* JetTrackSelectionTool : added
	* tag JetRecTools-00-02-01

2014-02-26 S. Schramm
	* TrackVertexAssociationTool: now uses IJetExecuteTool interface
	* no tag yet

2014-02-25 S. Schramm
	* Added TrackVertexAssociationTool
	* no tag yet

2014-02-19    <delsart@localhost>
	* Cleaned for run2
	* tag JetRecTools-00-02-00
	* last run1 tag was JetRecTools-00-01-61
