/**

@mainpage JetRecTools


@section Content

JetRecTools contains all JetAlgTool which depend on packages external to the Jet/ realm.

It contains all tools accessing calorimeter information, track information or dealing with truth particles.


@htmlinclude used_packages.html

@include requirements

*/
